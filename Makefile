.PHONY: dump
.DELETE_ON_ERROR:

all: dump

LOST_TABLES=abuse_reports \
	users \
	routes
TABLES_TO_KEEP_FROM_PROD=uploads chat_teams

# This set needs to load *before* the LOST_TABLES to fulfill their IR:
REACHABLE_TABLES_1=namespaces

# This set needs to load *after* the LOST_TABLES (or doesn't care either way):
REACHABLE_TABLES_2=labels appearances application_settings audit_events award_emoji projects boards broadcast_messages chat_names ci_builds ci_runner_projects ci_runners ci_trigger_requests ci_triggers ci_variables deploy_keys_projects deployments emails environments events forked_project_links identities issue_metrics keys label_links label_priorities lfs_objects lfs_objects_projects lists members merge_request_diffs merge_requests merge_request_metrics merge_requests_closing_issues milestones notes notification_settings oauth_access_grants oauth_access_tokens oauth_applications oauth_openid_requests pages_domains project_authorizations project_features project_group_links project_import_data project_statistics protected_branches protected_branch_merge_access_levels protected_branch_push_access_levels releases schema_migrations sent_notifications services snippets spam_logs subscriptions taggings tags timelogs todos trending_projects u2f_registrations user_agent_details users_star_projects web_hooks

MANUALLY_RECOVERED_TABLES=ci_pipelines issues personal_access_tokens 
PHANTOM_TABLES=ci_commits

SALVAGEABLE_TABLES=$(REACHABLE_TABLES) $(REACHABLE_TABLES_2) $(patsubst %, %_sauv, $(MANUALLY_RECOVERED_TABLES))
DUMPS=$(patsubst %, restore-tmp/%.sql, $(SALVAGEABLE_TABLES))

DUMPED_FROM_PROD_TABLES=$(LOST_TABLES) $(TABLES_TO_KEEP_FROM_PROD)
REMOTE_DUMPS=$(patsubst %, restore-remote/%.sql, $(DUMPED_FROM_PROD_TABLES))

dump: $(DUMPS)

remote-dump: $(REMOTE_DUMPS)

$(DUMPS): restore-tmp/%.sql:
	@mkdir restore-tmp || true
	LANG= pg_dump -t $* -h /home/quatrava/restore-pg -U root --column-inserts gitlabhq_crashed > $@

$(REMOTE_DUMPS): restore-remote/%.sql:
	@mkdir restore-remote || true
	ssh -p 2202 root@gitlab.epfl.ch docker exec gitlabdocker_postgresql_1 pg_dump -U gitlab -t $* --column-inserts gitlabhq_production > $@

clean:
	-rm -rf restore-tmp restore-remote restored

restore.sql:  $(DUMPS) $(REMOTE_DUMPS)
	cat $(patsubst %, restore-tmp/%_sauv.sql, $(MANUALLY_RECOVERED_TABLES)) | perl -pe 's/_sauv\b//g; s/OWNER TO (root|gitlab-psql|"gitlab-psql")/OWNER TO "gitlab"/' > $@
	cat $(patsubst %, restore-tmp/%.sql, $(REACHABLE_TABLES_1)) $(REMOTE_DUMPS) $(patsubst %, restore-tmp/%.sql, $(REACHABLE_TABLES_2)) >> $@
# Squish referential integrity problems.
# Empirically it looks like the only reason they arise is for new users having
# been granted permissions to projects.
	perl -i -pe 'next unless m/Name: fk_rails_11e7aa3ed9/; print qq(DELETE FROM project_authorizations WHERE user_id NOT IN (SELECT id FROM users);\n);' $@

POSTSERVER_ARGS = -D $(shell pwd)/restored
POSTSINGLE = postgres --single $(POSTSERVER_ARGS)
DB_RESTORED = gitlabhq_restored

restored: restore.sql Makefile
	@ (rm -rf restored; mkdir -p restored) || true
	initdb $@
# CREATE EXTENSION pr_trgm below, requires 'yum install postgresql-contrib.x86_64':
	mkdir -p $@/usr/share/pgsql/extension
# And also http://dba.stackexchange.com/a/61215/35786 :
	cp -a /usr/share/pgsql/extension/* $@/usr/share/pgsql/extension/
	echo "CREATE EXTENSION pg_trgm;" | $(POSTSINGLE) template1
	echo "CREATE DATABASE $(DB_RESTORED)" | $(POSTSINGLE) template1
	echo 'CREATE USER "gitlab-psql" CREATEDB CREATEUSER' | $(POSTSINGLE) template1
	echo "CREATE USER gitlab" | $(POSTSINGLE) template1
	( postgres $(POSTSERVER_ARGS) -k $(shell pwd)/restored & trap "while kill $$!; do sleep 1; done" EXIT HUP KILL TERM INT; sleep 2; psql -h $(shell pwd)/restored -U gitlab-psql gitlabhq_restored -f $< )

restored/pg_hba.conf: pg_hba.conf restored 
	cp $< $@

restored/postgresql.conf: postgresql.conf restored 
	cp $< $@

serve-restored: restored/pg_hba.conf
	postgres $(POSTSERVER_ARGS) -k $(shell pwd)/restored
