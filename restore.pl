#!/usr/bin/perl -w

use strict;
use autodie;
use vars qw($insert $parsed);   # Parsed INSERT passed to all the subs for cat_sql
use SQL::Parser;
use Tie::IxHash;
use YAML;

print "BEGIN WORK";

sub cat_sql($;&);  # See below

cat_sql "projects", sub {
  # die YAML::freeze($insert);
  $insert->{id} > 146
};





exit 0; ##############################################################


sub cat_sql($;&) {
  my ($sql_file, $opt_filter) = @_;
  local *SQL_FILE;
  open(SQL_FILE, "<", "restore-tmp/$sql_file.sql");
  while(<SQL_FILE>) {
    unless ($opt_filter && m/^INSERT INTO/) {
      print;
    } else {
      s/^INSERT INTO (\W+)_sauv /INSERT INTO $1/;
      while(! m/\);$/) {
        $_ .= <SQL_FILE>;
      }
      my $line = $_;

      local $_ = $line;
      local ($insert, $parsed) = parse_insert($line);
      next if $opt_filter->($insert, $parsed);
      print $line;
    }
  }
}

sub parse_insert {
  my ($insert_line) = @_;
  my $parser = SQL::Parser->new();
  $parser->parse($insert_line) or die "Cannot parse line $insert_line";
  my $struct = $parser->structure;
  my @keyvals;
  foreach my $i (0..$#{$struct->{values}->[0]}) {
    push @keyvals,
      $struct->{column_defs}->[$i]->{value},
      $struct->{values}->[0]->[$i]->{value};
  }
  tie my %ixhash, "Tie::IxHash", @keyvals;
  return (\%ixhash, $struct);
}

